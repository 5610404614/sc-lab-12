package HomeworkScoreAverage;

public class Average {
	private String name;
	private double total;
	private int amount;
	
	public Average(String name, double total, int amount){
		this.name = name;
		this.total = total;
		this.amount = amount;
	}
	
	public String getAverage(){
		return "\t " + name + "\t\t      " + total/amount;
	}
	
}
