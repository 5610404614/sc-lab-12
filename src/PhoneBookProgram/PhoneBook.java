package PhoneBookProgram;

public class PhoneBook {
	private String name;
	private String number;
	
	public PhoneBook(String name, String number){
		this.name = name;
		this.number = number;
	}
	
	public String getPhoneNumber(){
		return "\t " + name + "\t\t " + number;
	}
}
