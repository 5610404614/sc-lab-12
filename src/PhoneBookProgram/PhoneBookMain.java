package PhoneBookProgram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBookMain {

	public static void main(String[] args) {
		try {
			FileReader fileReader = new FileReader("phonebook.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			
			System.out.println("Open file:");
			System.out.println("\t     Java Phone Book \n");
			System.out.println("\t Name \t\t PhoneNumber");
			String line = buffer.readLine();
						 
			while (line != null) {
				String[] m = line.split(",");
				String name = m[0].trim();
				String number = m[1].trim();
				
				PhoneBook p = new PhoneBook(name, number);
				System.out.println(p.getPhoneNumber());				
				line = buffer.readLine();
			}
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
	}
}
