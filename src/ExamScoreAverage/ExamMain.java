package ExamScoreAverage;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ExamMain {
	public static void main(String[] args) {
		FileWriter fileWriter = null;
	 	 try {
			 // read from user
			 InputStreamReader inReader = new InputStreamReader(System.in);
			 BufferedReader buffer = new BufferedReader(inReader);
			 // write to file
			 fileWriter = new FileWriter("exam.txt");
			 PrintWriter out = new PrintWriter(fileWriter);
			 System.out.println("Input text to file:");
			 String line = buffer.readLine();
			 while (!line.equals("bye")) {					 
				 Exam e = new Exam(line);
				 String s = e.getScore();
				 out.println(s);
				 line = buffer.readLine();
				 
			 }		 	
			 out.flush();
	 	 }
	 	 catch (IOException e){
			 System.err.println("Error reading from user");
	 	 }
	}
}
