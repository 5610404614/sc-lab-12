package ExamScoreAverage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import HomeworkScoreAverage.Average;

public class ExamAverageMain {
	public static void main(String[] args) {
		FileWriter fileWriter = null;
		  try {
		      FileReader fileReader = new FileReader("exam.txt");
		      BufferedReader buffer = new BufferedReader(fileReader);
		    
		    
		      fileWriter = new FileWriter("average.txt",true);
		      PrintWriter out = new PrintWriter(fileWriter);
		      
		      out.println("\n\t>>>> Average Exam Score <<<<\n");
		      out.println("\t Name \t\t Average score");
		      String line = buffer.readLine();
		      while (line != null) {
		       String[] m = line.split(",");
		       String name = m[0].trim();
		       int amount = (m.length-1);
		       double sum = 0;
		       for(int i=1;i<m.length;i++){
		        sum += Double.parseDouble(m[i]);
		       }
		       
		       Average a = new Average(name, sum, amount);
		       out.println(a.getAverage());   
		       line = buffer.readLine();
		      }    
		      out.flush();
		       }
		     		       
		       catch (IOException e){
		       System.err.println("Error reading from file");
		       }
	}
}
